# JuiceShop

Example gitlab-ci job of running [GitLab's Browser Based DAST](https://docs.gitlab.com/ee/user/application_security/dast/browser_based.html) with authentication against [OWASP JuiceShop](https://owasp.org/www-project-juice-shop/).